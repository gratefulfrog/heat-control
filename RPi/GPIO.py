#!/usr/bin/python3

""" RPi GPIO stub
"""

BCM = 'BCM'
HIGH = 'HIGH'
LOW = 'LOW'
OUT = 'OUTPUT'


class PIN:
    mode = None
    dict = {} # key:[dir,val]
    def __init__(self):
        pass
        
    def setMode(mmod):
        PIN.mode = mmod
        print('Pin mode set to: ' + str(PIN.mode))

    def setup(id,dir):
        try:
            PIN.dict[id]
        except:
             PIN.dict[id]= [None,None]
        PIN.dict[id][0]=dir

    def output(id,val):
        PIN.dict[id][1] = val
        print('pin: ' + str(id) + ': val set to: '+ str( val))


def setmode (m):
    PIN.setMode(m)

def setup(pId,dir):
    PIN.setup(pId,dir)

def output(pId,val):
    if val == 'HIGH':
        res = 1
    elif val == 'LOW':
        res = 0
    else:
        res = val
    PIN.output(pId,res)

def input(pId):
    return PIN.dict[pId][1]
    
def show():
    res  = 'mode: ' + str(PIN.mode) + '\n'
    for id in PIN.dict.keys():
        res += 'pin:'   + str(id) + ': val: ' + str(PIN.dict[id]) + '\n'
    print(res)

def cleanup():
    print('\nGPIO cleaned up, thanks for thinking of us!')
    
    
    
        
        
