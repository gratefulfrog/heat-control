# Heat Control

_For Art's sake and nothing else!_

## To make this work on **RPi** :

1. Only use the parallel version! The sequential version is untested!

1. Copy the file `pheater.py`  to a directory, for example:

   `/home/pi/pheater.py`

   DO NOT COPY THE RPi directory! It is only there to provide stubs for off-board  testing!!!

1. Edit the file `pheater.py` so that the pins and wake and sleep times are as you wish.

1. Then, using `sudo`  copy the file rc.local to the /etc directory

   `$ sudo cp rc.local /etc`

1. Now pheater.py will be excuted upon rpi startup!
   


## To make this work on **Arduino**:

1. Using the Arduino IDE, open the file `heat-control/arduino_pheater/arduino_pheater.ino`

1. Edit the values in `pinVec[], wakeTimeVec[], sleepTimeVec[]` to your needs.

1. Then compile and upload as usual. It should work, and as of 2020 09 22 it has been tested on an Arduino UNO.


## To test the parallel version on a **Linux PC**:

1. First set the values for the three vectors to your needs in the file `heat-control/arduino_pheater/arduino_pheater.ino`

1. At the command line,  `cd heat-control/ArduinoStubs` Directory

1. compile using make:

   `$ make`

1. and run it:

   `$ ./a.out`

1. If needed, there is a `clean` target in the makefile as well.

1. It is also possible to make and run in one go:

   `$ make run`

