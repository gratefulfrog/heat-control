// tester for pin operations

#include "PIN.h"

int main(){
  // PIN::pinVec = new PIN*[20];
  PIN p1 = PIN(0),
    p2 = PIN(12);

  digitalWrite(0,1);
  digitalWrite(12,1);
  showPins();
  digitalWrite(0,0);
  digitalWrite(12,0);
  showPins();
  
}
