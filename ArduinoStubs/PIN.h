#ifndef PIN_H
#define PIN_H

#include <stdio.h>

// arduino pin stubs

#define INPUT  (0)
#define OUTPUT (1)
#define A0     (14)
#define A1     (15)
#define A2     (16)
#define A3     (17)
#define A4     (18)
#define A5     (19)

#define NB_PINS (20)

class PIN;

class PIN{
public:
  static PIN* pinVec[];
  static bool init;
  static const char* reportPrefix;

private:
  const int pin;
  int mode,
    val;
  const char* valAsString() const;
  
public:
  PIN(int p);
  void setMode(int);
  int read() const;
  void write(int);
  void show() const;
};

extern void pinMode(int p,int mode);
extern void digitalWrite(int p, int v);
extern int  digitalRead(int p);
extern void showPins();

#endif
