#include "PIN.h"

// arduino pin stubs

PIN* PIN::pinVec[NB_PINS];

bool PIN::init = false;

const char* PIN::reportPrefix = "PIN STUB REPORT:";

PIN::PIN(int p):pin(p){
  if (!init){
    for (int i=0;i<NB_PINS;i++){
      pinVec[i] = NULL;
    }
    init = true;
  }
  pinVec[pin] = this;
  val=0;
  mode = -1;
  printf("%s pin %d created!\n",PIN::reportPrefix,pin);
}

void PIN::setMode(int mmode){
  mode = mmode;
  printf("%s pin %d mode set to %d\n",PIN::reportPrefix,pin,mode);
}

int PIN::read()const{
  return val;
}

const char* PIN::valAsString() const{
  return read() ? "ON" : "OFF";
}

void PIN::write(int v){
  val = v;
  printf("%s pin %d %s!\n",PIN::reportPrefix, pin,valAsString());
}

void PIN::show() const{
  printf("%s pin %d %s\n",PIN::reportPrefix,pin,valAsString());
}

void pinMode(int p,int mode){
  PIN::pinVec[p]->setMode(mode);
}
void digitalWrite(int p, int v){
  PIN::pinVec[p]->write(v);
}

int  digitalRead(int p){
  return PIN::pinVec[p]->read();
}

void showPins(){
  printf("%s All Pins\n",PIN::reportPrefix);
  for (int i=0;i<NB_PINS;i++){
    if (PIN::pinVec[i]){
      PIN::pinVec[i]->show();
    }
  }
}
