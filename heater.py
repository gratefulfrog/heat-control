#!/usr/bin/python3

"""
traduit depuis l'arduino
// constants won't change. They're used here to 
// set pin numbers:

const int buttonPin = 2;     // the number of the pushbutton pin
const int heatPin1 =  10;      // the number of the LED pin
const int heatPin2 =  11;      // the number of the LED pin
const int heatPin3 =  8;      // the number of the LED pin
const int heatPin4 =  9;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status


void setup() {
  // initialize the LED pin as an output:
  pinMode(heatPin1, OUTPUT);      
  pinMode(heatPin2, OUTPUT);
  pinMode(heatPin3, OUTPUT);      
  pinMode(heatPin4, OUTPUT);       

  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);     

}


void loop(){
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed.
  // if it is, the buttonState is HIGH:
  if (buttonState == LOW) {     
    // start the heat sequence. as it is using delay, while the sequence is going, it does not detect the button
  
    digitalWrite(heatPin1, HIGH);
    delay(3000); // you can change the timing as you like, 1sec=1000
    digitalWrite(heatPin1, LOW);

    digitalWrite(heatPin2, HIGH);
    delay(3000); // you can change the timing as you like, 1sec=1000
    digitalWrite(heatPin2, LOW);

    digitalWrite(heatPin3, HIGH);
    delay(3000); // you can change the timing as you like, 1sec=1000
    digitalWrite(heatPin3, LOW);


    digitalWrite(heatPin4, HIGH);
    delay(3000); // you can change the timing as you like, 1sec=1000
    digitalWrite(heatPin4, LOW);
  } 
  else {
    // turn both the heating off
    digitalWrite(heatPin1, LOW);
    digitalWrite(heatPin2, LOW);
    digitalWrite(heatPin3, LOW);
    digitalWrite(heatPin4, LOW);
  }
}
""" 


import time
import RPi.GPIO as GPIO

# changer les pins pour le vrais!!!
pinVec = [6]
wakeTime = 60
sleepTime = 5
   
def init():
    GPIO.setmode(GPIO.BCM)
    for p in pinVec:
        GPIO.setup(p, GPIO.OUT)

def loop():
    try:
        while True:
            for p in pinVec:
                print('pin: ',p,' ON!')
                GPIO.output(p,GPIO.HIGH)
                time.sleep(wakeTime)
                print('pin: ',p,' OFF!')
                GPIO.output(p,GPIO.LOW)
                time.sleep(sleepTime)

    except KeyboardInterrupt:
        print('\nBye!')

if __name__ == '__main__':
    init()
    loop()
