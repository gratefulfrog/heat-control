#!/usr/bin/python3

# changer les PINS AND TIMES pour le vrais!!!

# ce sont les pins à controller en parallel
pinVec = [26,13]

# le temps en seconds pendant lesquels le pin correspondant sera ON
wakeTimeVec = [10,10]

# le temps en seconds pendant lesquels le pin correspondant sera OFF
sleepTimeVec = [5,6]  


import time
import RPi.GPIO as GPIO

def now():
    return time.time()

class OnOff:
    def __init__(self,pin,onTime,offTime):
        """ pin should already be initialized as output,
        """
        self.pin = pin
        GPIO.output(self.pin,GPIO.LOW)
        self.lastChangeTime = now()
        self.onOffTimeVec = [offTime,onTime]

    def update(self):
        currentPinVal = int(GPIO.input(self.pin))
        if now()-self.lastChangeTime >= self.onOffTimeVec[currentPinVal]:
            print('pin: ',self.pin, 'OFF' if currentPinVal else 'ON')
            GPIO.output(self.pin,int(not currentPinVal))
            self.lastChangeTime = now()
            
                        
class ParallelHeater:   
    def __init__(self):
        self.ooVec = []
        GPIO.setmode(GPIO.BCM)
        for i in range(len(pinVec)):
            GPIO.setup(pinVec[i], GPIO.OUT)
            self.ooVec += [OnOff(pinVec[i],wakeTimeVec[i],sleepTimeVec[i])]

    def loop(self):
        while True:
            for o in self.ooVec:
                o.update()


if __name__ == '__main__':
    try:
        ph = ParallelHeater()
        ph.loop()
    except KeyboardInterrupt:
        print('\nBye!')
    except Exception as e:
        print(e)
    GPIO.cleanup()
