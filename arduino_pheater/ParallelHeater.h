#ifndef PARALLELHEATER_H
#define PARALLELHEATER_H

#if !STUBIT
#include <Arduino.h>
#else
#include <stdio.h>
#endif
#include "OnOff.h"


class ParallelHeater{
  private:
    OnOff **ooVec;
    const int nbPins;
  public:
    ParallelHeater(int nnbPins, int pinVec[],int wakeTimeVec[],int sleepTimeVec[]);
    void update() const;
};

#endif
