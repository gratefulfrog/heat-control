#include "outils.h"

unsigned long now(){
#if STUBIT
  struct timeval tv;
  gettimeofday(&tv, NULL);

  unsigned long long millisecondsSinceEpoch =
    (unsigned long long)(tv.tv_sec) * 1000 +
    (unsigned long long)(tv.tv_usec) / 1000;
  return (unsigned long)millisecondsSinceEpoch/1000;
#else
  return millis()/1000;
#endif
}
