#ifndef ONOFF_H
#define ONOFF_H

#if !STUBIT
#include <Arduino.h>
#else
#include <stdio.h>
#include "../ArduinoStubs/PIN.h"
#endif

#include "outils.h"

class OnOff{
  private:
    const int pin,
              offOnTimeVec[2];
   unsigned long lastChangeTime;
  public:
    OnOff(int ppin, int onTime, int offTime);
    void update();
};

#endif
