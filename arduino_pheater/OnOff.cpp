#include "OnOff.h"

OnOff::OnOff(int ppin, int onTime, int offTime): pin(ppin),offOnTimeVec{offTime,onTime}{
#if STUBIT
  PIN *xp = new PIN(pin);
#endif
  pinMode(pin,OUTPUT);
  digitalWrite(pin,0);
  lastChangeTime = now();
}

void OnOff::update(){
  int currentPinVal = digitalRead(pin);
  unsigned long deltaT = now()-lastChangeTime;
  if (deltaT >= offOnTimeVec[currentPinVal]){
#if STUBIT
    printf("Turning pin: ");
    printf("%d",pin);
    printf(currentPinVal ? " OFF" : " ON");
    printf("\n");
#else
    Serial.print("Turning pin: ");
    Serial.print(pin);
    Serial.println(currentPinVal ? " OFF" : " ON");
#endif
    digitalWrite(pin,!currentPinVal);
    lastChangeTime = now();
    digitalWrite(LED,!digitalRead(LED));
  }
}
