#include "ParallelHeater.h"

int pinVec[]       = {5,6},
    wakeTimeVec[]  = {10,10},
    sleepTimeVec[] = {5,6};

ParallelHeater *ph;

#if STUBIT
#include <stdio.h>
int setup() {
   printf("testing\n");  
   PIN *xp = new PIN(LED);
#else
void setup() {
  Serial.begin(9600);
  Serial.println("testing...");  
#endif
  pinMode(LED,OUTPUT);
  digitalWrite(LED,0);

  if ((sizeof(pinVec) == sizeof(wakeTimeVec)) && (sizeof(wakeTimeVec) == sizeof(sleepTimeVec))){
#if STUBIT
    printf("Vectors OK!\n");
#else
    Serial.println("Vectors OK!");
#endif
  }
  else{
#if STUBIT
    printf("Vector lengths mismatch... aborting!\n");
    return 1;
#else
    Serial.println("Vector lengths mismatch... aborting!");
#endif
    while(1);
  }
#if STUBIT  
  printf("Allocating ParallelHeater\n");
#else
  Serial.print("Allocating ParallelHeater\n");
#endif
  ph = new ParallelHeater(sizeof(pinVec)/sizeof(int),pinVec,wakeTimeVec,sleepTimeVec);
  if (!ph){   
#if STUBIT
    printf("ParralelHeater allocation failure... aborting!");
    return 1;
#else
    Serial.println("ParralelHeater allocation failure... aborting!");
#endif
    while(1);
  }
  else{
#if STUBIT
    printf("ParallellHeater allocation SUCCESS!\n");
#else
    Serial.println("ParallellHeater allocation SUCCESS!");
#endif
  }
#if STUBIT
    printf("Starting up... \n");
    return 0;
#else
    Serial.println("Starting up...");
#endif

}

void loop() {
  ph->update();
}

#if STUBIT
int main(){
  printf("starting...\n");
  if (!setup()){
    printf("looping\n");
    while (1){
      loop();
    }
    return 0;
  }
  return 1;
}
#endif
