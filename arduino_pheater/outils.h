#ifndef OUTILS_H
#define OUTILS_H

#define LED (13)

#if !STUBIT
#include <Arduino.h>
#else
#include <stdio.h>
#include <sys/time.h>
#endif


extern unsigned long now();

#endif
