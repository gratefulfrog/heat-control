#include "ParallelHeater.h"


ParallelHeater::ParallelHeater(int nnbPins, 
			       int pinVec[],
			       int wakeTimeVec[],
			       int sleepTimeVec[]):nbPins(nnbPins){
  // vector lengths have been chacked to be all the same at this point
  ooVec = new OnOff*[nbPins];
  if (!ooVec){
#if STUBIT
    printf("Failed to allocate OnOffVec... aborting\n");
#else
    Serial.println("Failed to allocate OnOffVec... aborting");
#endif
    while(1);
  }
  
#if STUBIT
    printf("allocated ooVec for %d pins\n",nbPins);
#else
    Serial.print("allocated ooVec for ");
    Serial.print(nbPins);
    Serial.print(" pins\n");
#endif
  
  for (int i=0;i<nbPins;i++){
    ooVec[i] = new OnOff(pinVec[i],wakeTimeVec[i],sleepTimeVec[i]);
    if (!ooVec[i]){
#if STUBIT
    printf("Failed to allocate OnOff instance... aborting");
#else
      Serial.println("Failed to allocate OnOff instance... aborting");
#endif
      while(1);  
    }
  }
#if STUBIT
  printf("instanciated ParallelHeater\n");
#else
  Serial.println("instanciated ParallelHeater");
#endif
 }

void ParallelHeater::update() const{
  for (int i=0;i<nbPins;i++){
    ooVec[i]->update();
  }
}
